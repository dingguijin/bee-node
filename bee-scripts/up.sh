#!/bin/sh

if [ -n "$1" ]; then
    echo "Bee instance index: " $1
else
    echo "No bee instance index"
    exit 1
fi

tmux new-session -d -s "bee-${1}" "sudo ./run.sh ${1}"

echo "./attach.sh ${1} to see the output, 'ctrl+b d' to detatch, 'ctrl+c' to exit."
