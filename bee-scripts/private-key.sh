#!/bin/sh

if [ -n "$1" ]; then
    echo "Bee instance index: " $1
else
    echo "No bee instance index"
    exit 1
fi

PASS="ThisIsPassword"
echo "Export Bee instance $1 keys"

BEE_DATA_=/var/lib/bee/bee-data-${1}

sudo ../deps-binary/export-swarm-key-linux-amd64 ${BEE_DATA_}/keys/ ${PASS}
