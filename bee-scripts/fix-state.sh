#!/bin/sh

if [ -n "$1" ]; then
    echo "Bee instance index: " $1
else
    echo "No bee instance index"
    exit 1
fi

echo "Run Bee instance $1"

INDEX=$(expr 5 \* $1)

BEE_DATA_=/var/lib/bee/bee-data-${1}
mkdir -p ${BEE_DATA_}

BASE_ADDR=$(expr 1633 + $INDEX)
BASE_ADDR_INC=$(expr 1633 + $INDEX + 1)
BASE_ADDR_INC_INC=$(expr 1633 + $INDEX + 2)

#SWAP_ENDPOINT=https://rpc.slock.it/goerli 
#SWAP_ENDPOINT="http://192.168.50.141:8545"
SWAP_ENDPOINT="http://192.168.0.133:8545"

rm -rf ${BEE_DATA_}/statestore
