#!/usr/bin/env sh

if [ -n "$1" ]; then
    echo "Range begin ${1}" > /dev/null
else
    echo "No range begin"
    exit 1
fi

if [ -n "$2" ]; then
    echo "Range end ${2}" > /dev/null
else
    echo "No range end"
    exit 1
fi

for i in $(seq $1 $2)
do
	tmux kill-session -t "bee-${i}"
done

