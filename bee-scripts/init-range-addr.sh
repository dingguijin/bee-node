#!/bin/sh

# seq is close region

if [ -n "$1" ]; then
    EB=$1
else
    echo "No range begin"
    exit 1
fi

if [ -n "$2" ]; then
    EE=$2
else
    echo "No range end"
    exit 1
fi

for i in $(seq $1 $2)
do
	INIT_LOG=$(./init.sh ${i})
	ADDR_LOG=$(echo ${INIT_LOG} | grep -o -e "using ethereum address \(.*\)\"" | sed "s/using ethereum address /0x/g" | sed "s/\"//g")
	echo ${ADDR_LOG}
done

