#!/bin/sh

if [ -n "$1" ]; then
    echo "Bee instance index: " $1
else
    echo "No bee instance index"
    exit 1
fi

if [ -n "$2" ]; then
    echo "db_capacity: " $2
    DB_CAP=$2
else
    DB_CAP=2000000
fi

echo "Run Bee instance $1"

INDEX=$(expr 5 \* $1)

BEE_DATA_=/var/lib/bee/bee-data-${1}
mkdir -p ${BEE_DATA_}

BASE_ADDR=$(expr 1633 + $INDEX)
BASE_ADDR_INC=$(expr 1633 + $INDEX + 1)
BASE_ADDR_INC_INC=$(expr 1633 + $INDEX + 2)

#SWAP_ENDPOINT=https://rpc.slock.it/goerli 
#SWAP_ENDPOINT="http://192.168.50.141:8545"
#SWAP_ENDPOINT="http://192.168.0.133:8545"
SWAP_ENDPOINT="http://116.63.188.156:8545"

../deps-binary/bee start --full-node=true --swap-deployment-gas-price=322 --cache-capacity=${DB_CAP} --api-addr=:${BASE_ADDR} --p2p-addr=:${BASE_ADDR_INC} --debug-api-enable=true --debug-api-addr=:${BASE_ADDR_INC_INC} --password=ThisIsPassword --clef-signer-enable=false --data-dir=${BEE_DATA_} --swap-enable=true --swap-endpoint=${SWAP_ENDPOINT} --verbosity=5 --welcome-message="I am your Bzz ${1}"
