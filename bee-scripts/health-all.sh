#/usr/bin/env sh

if [ -n "$1" ]; then
    echo "Bee range: ${1} "
else
    echo "No bee instance range"
    exit 1
fi

for i in $(seq 1 $1)
do
	#echo ${i}
	./health.sh ${i}
done
