#!/bin/sh

if [ -n "$1" ]; then
    echo "Bee instance index: " $1
else
    echo "No bee instance index"
    exit 1
fi

echo "Run Bee instance $1"


INDEX=$(expr 5 \* $1)

BEE_DATA_=/var/lib/bee/bee-data-${1}

BASE_ADDR=$(expr 1633 + $INDEX)
BASE_ADDR_INC=$(expr 1633 + $INDEX + 1)
BASE_ADDR_INC_INC=$(expr 1633 + $INDEX + 2)

#RANDOM_=${RANDOM:0:1}
RANDOM_=`shuf -i 1-10 -n 1`

echo $RANDOM_
echo $RANDOM_
echo $RANDOM_

COUNT=$(expr ${RANDOM_} + 1)
echo $COUNT
dd if=/dev/urandom of=/tmp/test.txt bs=1024k count=${COUNT}
curl -F file=@/tmp/test.txt http://localhost:${BASE_ADDR}/files

