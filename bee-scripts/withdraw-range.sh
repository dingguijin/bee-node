#!/bin/sh

if [ -n "$1" ]; then
    echo "Range begin: " $1
else
    echo "No range begin"
    exit 1
fi

if [ -n "$2" ]; then
    echo "Range end: " $2
else
    echo "No range end"
    exit 1
fi

for i in $(seq $1 $2)
do
    ./withdraw.sh ${i}
done

