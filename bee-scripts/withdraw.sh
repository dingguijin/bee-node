#!/bin/sh

if [ -n "$1" ]; then
    echo "Bee instance index: " $1
else
    echo "No bee instance index"
    exit 1
fi

INDEX=$(expr 5 \* $1)

BASE_ADDR=$(expr 1633 + $INDEX)
BASE_ADDR_INC=$(expr 1633 + $INDEX + 1)
BASE_ADDR_INC_INC=$(expr 1633 + $INDEX + 2)

x=$(curl -s http://localhost:${BASE_ADDR_INC_INC}/chequebook/balance | jq ".availableBalance")
w=90000000000000000

echo ${x}

y=$(echo "${x}" |awk '{printf("%d",$0)}') 

if [ -n "$x" ]
then
	echo "has instance " $1
else
	echo "no instance " $1
	exit 1
fi

if [ $y -gt $w ]; then
	curl -s -XPOST http://localhost:${BASE_ADDR_INC_INC}/chequebook/withdraw?amount=${w}
else
	echo "already withdraw"
fi
