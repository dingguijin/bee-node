#/usr/bin/env sh

if [ -n "$1" ]; then
    echo "Remove DB Bee index: " $1
else
    echo "No bee instance index"
    exit 1
fi

DB_DIR=/var/lib/bee/bee-data-${1}/localstore/*

rm -rf ${DB_DIR}
