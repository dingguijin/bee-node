#/usr/bin/env sh

if [ -n "$1" ]; then
    echo "Cashout Bee index: " $1
else
    echo "No bee instance index"
    exit 1
fi

INDEX=$(expr 5 \* $1)


BASE_ADDR=$(expr 1633 + $INDEX)
BASE_ADDR_INC=$(expr 1633 + $INDEX + 1)
BASE_ADDR_INC_INC=$(expr 1633 + $INDEX + 2)

DEBUG_API=http://localhost:${BASE_ADDR_INC_INC}
MIN_AMOUNT=1000

curl -s "$DEBUG_API/chequebook/cheque" | jq -r '.lastcheques | .[].peer'
